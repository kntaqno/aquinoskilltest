<?php
require 'config.php';
if(!empty($_SESSION["id"])){
  header("Location: index.php");
}
if(isset($_POST["submit"])){
  $username = $_POST["username"];
  $email = $_POST["email"];
  $password = $_POST["password"];
  $confirmpassword = $_POST["confirmpassword"];
  $duplicate = mysqli_query($conn, "SELECT * FROM db_user WHERE username = '$username' OR email = '$email'");
  if(mysqli_num_rows($duplicate) > 0){
    echo
    "<script> alert('Username or Email Has Already Taken'); </script>";
  }
  else{
    if($password == $confirmpassword){
      $query = "INSERT INTO db_user VALUES('','$username','$email','$password')";
      mysqli_query($conn, $query);
      echo
      "<script> alert('Registration Successful'); </script>";
    }
    else{
      echo
      "<script> alert('Password Does Not Match'); </script>";
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <style>
* {
  box-sizing: border-box;
  font-family: Arial;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  background-color:#4169E1;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

h2 {
  background-color: #4169E1;
  color: white;
}

.center {
  border: 1px solid;
  display: block;
  width: 50%;
  justify-content: center;
}

button {
  float: left;
  background: rgb(35, 174, 202);
  padding: 10px 30px;
  color: #fff;
  border-radius: 3px;
  margin-right: 5px;
  border: none;
}

a:hover, a:link {
  color: #FFA833;
  background-color: transparent;
  text-decoration: none;
}

</style>
  </head>
  <body>
    <div class="row">
    <div class="column">
        <h2>MiniBlog</h2>
    </div>
    <div class="column">
    <div align="right">
        <h2>Login</h2>
    </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <h3>Registration</h3>
    <div class="center">
            <h3>See the Registration Rules</h3>
    </div>
    <div class="center">
    <form class="" action="" method="post" autocomplete="off">
        <br>
        <br>
      <input type="text" name="username" id = "username" required value="" required placeholder="Enter Username">
        <br>
        <br>
        <br>
      <input type="email" name="email" id = "email" required value="" required placeholder="Enter Email">
        <br>
        <br>
        <br>
      <input type="password" name="password" id = "password" required value="" required placeholder="Enter Password">
        <br>
        <br>
        <br>
      <input type="password" name="confirmpassword" id = "confirmpassword" required value="" required placeholder="Confirm Password">
        <br>
        <br>
      <button type="submit" name="submit">REGISTER</button>
    </form>
    <br>
    <br>
        <p>Return to the <a href="login.php">LOGIN PAGE</a>   &nbsp;&nbsp;&nbsp;&nbsp;.</p>
    </div>
  </body>
</html>