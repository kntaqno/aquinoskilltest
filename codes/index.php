<?php
require 'config.php';
if(!empty($_SESSION["id"])){
  $id = $_SESSION["id"];
  $result = mysqli_query($conn, "SELECT * FROM db_user WHERE id = $id");
  $row = mysqli_fetch_assoc($result);
}
else{
  header("Location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <style>
* {
  box-sizing: border-box;
  font-family: Arial;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  background-color:#4169E1;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

h2 {
  background-color: #4169E1;
  color: white;
}

.center {
  border: 1px solid;
  display: block;
  width: 50%;
  justify-content: center;
}

button {
  float: left;
  background: rgb(35, 174, 202);
  padding: 10px 30px;
  color: #fff;
  border-radius: 3px;
  margin-right: 5px;
  border: none;
}

a:hover, a:link {
  color: #FFA833;
  background-color: transparent;
  text-decoration: none;
}

</style>
  </head>
  <body>
    <div class="row">
    <div class="column">
        <h2>MiniBlog</h2>
    </div>
    <div class="column">
        <h2>Hi <?php echo $row["username"]; ?>! Home Logout</h2>
    </div>
    <br>
    <br>
        <br>
        <h3></h3>
    <div class="center">
      <h3>Post Title</h3>
        <p>Contents of the Post</p>
        <p>Date: <?php date_default_timezone_set("Asia/Hong_Kong"); echo date("jS \of F Y h:i:s A") . "<br>"; ?></p>
    </div>
    <div class="center">
    <br>
        <button type="submit" name="submit" style="background-color:red">DELETE</button>
        <button type="submit" name="submit" style="background-color:green">EDIT</button>
        <br>
        <br>
        <br>
        </div>
        <br>
        <div class="center">
    <br>
      <button type="submit" name="submit" style="background-color:blue">CREATE NEW POST</button>
      <br>
      <br>
      <br>
      </div>
    </form>
    
    </div>
  </body>
  </body>
  <br>
  <a href="logout.php">Logout</a>
</html>

